import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Bem vindo ao projeto DevOps.
        </p>
        <p>Realiza versionamento semântico.</p>
        <p>Agora faz deploy automático para AWS.</p>
      </header>
    </div>
  );
}

export default App;
