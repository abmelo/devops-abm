# devops-abm

- Projeto de exemplo das práticas DevOps.
  * Testando CI/CD de exemplo.
  * Excluído o job do pipeline que deu erro para ver se o próximo commit dispara a execução.
  * As variáveis para AWS_API estavam com nome errado, e faltou acesso de recurso para executar "ec2:ImportKeyPair".
  * Faltou acesso "secretsmanager:DescribeSecret"
  * Faltou acesso "secretsmanager:CreateSecret"
  * Usar o arquivo global.yml não funcionou no arquivo inventory.aws.aws_ec2.yml. Trocar pelo parâmetro desejado (regions:).
  * Estava usando o nome de usuário errado: usava `ec2_user` ao invés de `ec2-user`.
  * Necessário acrescentar uma linha em branco no final da chave recuperada da AWS Secrets Manager para evitar o erro `Load key "arquivo": invalid format`
  * Testar novamente com acesso mais restrito.